@extends('navbar')
@section('content')
    
<div class="section-container content-container">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="col-md-6">
                    <img src="./assets/images/me.jpeg" class="img-responsive" alt=""> 
                </div>
                <div class="col-md-6">
                    <h1 class="h2">Regina Tatang</h1>
                    <p style="text-align: justify">I was born and bred in Jakarta and I'm a student of Pendidikan Ganesha University, Bali.
                    Highly enthusiastic in everything anew around me, I'm also easy to adapt on new environments and usually have high interests when it comes to learning new things. 
                    I also like to try things that requires creativity.
                    </p>
                </div>

                <div class="col-md-6">
                        <h3 class="template-title-example">MORE ABOUT ME</h3>
                            <ul>
                            <li>Date of Birth : 12 Juli 2001</li>
                            <li>Place of Birth : Jakarta</li>
                            <li>Gender : Female </li>
                            <li>Marital status : Single </li>
                            </ul>
                </div>

                <div class="col-md-6">
                        <h3 class="template-title-example">Educational History</h3>
                            <ul>
                            <li>SDN Bintaro 04 Pagi</li>
                            <p>2007-2013</p>
                            <li>SMP Strada Bhakti Utama</li>
                            <p>2013-2016</p>
                            <li>SMAN 87 Jakarta</li>
                            <p>2016-2019</p>
                            <li>Marital status : Single </li>
                            </ul>
                </div>
                <div class="col-xs-12 col-md-6">
                    <h3 class="template-title-example">REACH ME THROUGH</h3>
                    
                    <div class="row">
                        <div class="col-md-6">
                                <ul class="list-icons">
                                    <li>
                                    <a href="https://web.facebook.com/regina.rere.984" class="social-round-icon fa-icon">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                    </li>
                                    <li>
                                    <a href="" class="social-round-icon fa-icon">
                                        <i class="fa fa-envelope-open-o"></i>
                                    </a>
                                    </li>
                                    <li>
                                    <a href="https://www.linkedin.com/in/regina-tatang-30469a1b3" class="social-round-icon fa-icon">
                                        <i class="fa fa-linkedin"></i>
                                    </a>
                                    </li>
                                    <li>
                                    <a href="https://instagram.com/reginatatang?utm_medium=copy_link" class="social-round-icon fa-icon">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                    </li>
                                </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection