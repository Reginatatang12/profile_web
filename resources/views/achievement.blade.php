@extends('navbar')
@section('content')
      
<div class="hero-full-container background-image-container white-text-container" style="background-image:url('./assets/images/GOP6.jpg')">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="hero-full-wrapper">
          <div class="text-content  text-center">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="section-container">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="text-center section-container-spacer">
          <h2 class="with-project-number"><span class="project-number">01</span>Night Photography</h2>
        </div>
        <div class="row section-container-spacer">
          <div class="col-md-4">
              <div class="section-container-spacer">
                <h3>Photographic Project</h3>
                <p>Menphis June, 2016</p>
              </div>

              <h4>Credits & Crew</h4>
              
              <ul class="list-unstyled">
                <li>Stanford</li> 
                <li>Pablo Malice</li>
                <li>Patricia Stano</li>
              </ul>
              

              <p>Cityzen circus Association</p>
              <p>Menphis City Hall</p>
          </div>
          <div class="col-md-8">
            <h3>How to exchange with the nocturnal light ?</h3>
            <p>Id eu nisl nunc mi ipsum faucibus vitae aliquet. Auctor urna nunc id cursus metus aliquam. A pellentesque sit amet
              porttitor eget dolor morbi. Interdum varius sit amet mattis. Tincidunt dui ut ornare lectus sit amet est placerat
              in. Diam volutpat commodo sed egestas. Praesent elementum facilisis leo vel fringilla est ullamcorper eget nulla.
              Non odio euismod lacinia at quis risus sed vulputate. Lacus sed turpis tincidunt id aliquet risus feugiat.</p>
              <p> Mieget mauris pharetra et ultrices neque ornare. Pellentesque elit ullamcorper dignissim cras tincidunt lobortis.
              Euismod in pellentesque massa placerat. Consectetur libero id faucibus nisl tincidunt eget. Pulvinar etiam non
              quam lacus suspendisse. Tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada. Ante in nibh mauris cursus
              mattis molestie a iaculis. Gravida cum sociis natoque penatibus et magnis dis parturient montes. Ultrices gravida
              dictum fusce ut placerat orci. Id cursus metus aliquam eleifend mi in. Odio eu feugiat pretium nibh ipsum consequat.
            </p>
          </div>
        </div>


        <img src="./assets/images/hmj1.jpeg" alt="" class="img-responsive">
        <p>Menphis city center</p>

        <blockquote class="text-center large-spacing">
            <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante."</p>
            <small class="pull-right">Irina Martin</small>
        </blockquote>
        
        <div class="row">
            <div class="col-md-6">
            <img src="./assets/images/img-02.jpg" class="img-responsive" alt="">
            <p>Menphis skyline</p>
            </div>
            <div class="col-md-6">
            <img src="./assets/images/img-01.jpg" class="img-responsive" alt="">
            <p>Artefact Buildging</p>
            </div>
        </div>

        <img src="./assets/images/img-04.jpg" class="img-responsive" alt="">
        <p>Menphis skyline</p>

      </div>
    </div>
  </div>
</div>

@endsection