<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="IE=edge" http-equiv="X-UA-Compatible">
  <meta content="width=device-width,initial-scale=1" name="viewport">
  <meta content="description" name="description">
  <meta name="google" content="notranslate" />
  <meta content="Mashup templates have been developped by Orson.io team" name="author">

  <!-- Disable tap highlight on IE -->
  <meta name="msapplication-tap-highlight" content="no">
  
  <link rel="apple-touch-icon" sizes="180x180" href="./assets/apple-icon-180x180.png">
  <link href="./assets/favicon.ico" rel="icon">
  
  <title>Regina page</title>  

<link href="./main.3da94fde.css" rel="stylesheet"></head>

<body>

<!-- Add your content of header -->

<header>
  <nav class="navbar navbar-fixed-top navbar-default">
    <div class="container">
        <button type="button" class="navbar-toggle">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        
      <nav class="navbar-fullscreen" id="navbar-middle">
        <button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <ul class="navbar-fullscreen-links">
          <li><a href="/index" title="">Home</a></li>
          <li><a href="/about" title="">About me</a></li>
          <li><a href="/achievement" title="">Achievements</a></li>
        </ul>

        <div class="footer-container">
           
          <p><small>© Untitled | Website created with <a href="http://www.mashup-template.com/" title="Create website with free html template">Mashup Template</a>/<a href="https://www.unsplash.com/" title="Beautiful Free Images">Unsplash</a></small></p>
          <p class="footer-share-icons">
              <a href="https://web.facebook.com/regina.rere.984" class="fa-icon" title="">
                  <i class="fa fa-facebook" aria-hidden="true"></i>
              </a>
              <a href="https://www.linkedin.com/in/regina-tatang-30469a1b3" class="fa-icon" title="">
                  <i class="fa fa-linkedin" aria-hidden="true"></i>
              </a>
              <a href="https://instagram.com/reginatatang?utm_medium=copy_link" class="fa-icon" title="">
                  <i class="fa fa-instagram" aria-hidden="true"></i>
              </a>
              <a href="https://api.whatsapp.com/send?phone=6281385612212" class="fa-icon" title="">
                  <i class="fa fa-whatsapp" aria-hidden="true"></i>
              </a>
          </p>                       
        </div>
      </nav>     
    </div>
  </nav>
</header>

@yield('content')

<script>
    document.addEventListener("DOMContentLoaded", function (event) {
      navbarFixedTopAnimation();
      closeMenuBeforeGoingToPage();
    });
  </script>  
  
  <script>
      document.addEventListener("DOMContentLoaded", function (event) {
        navbarToggleSidebar();
        closeMenuBeforeGoingToPage();
        navActivePage();
      });
  </script>
   <script type="text/javascript" src="./main.4c6e144e.js"></script>

</body>
</html>