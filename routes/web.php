<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('home');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/achievement', function () {
    return view('achievement');
});
*/

Route::get('home',[App\Http\Controllers\HomeController::Class,'index'])->name('home');
Route::get('about',[App\Http\Controllers\AboutController::Class,'profile'])->name('about');
Route::get('achievement',[App\Http\Controllers\AchievementController::Class,'prestasi'])->name('achievement');